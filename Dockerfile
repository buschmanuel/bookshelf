FROM gradle:latest AS build

COPY build.gradle.kts /tmp/
COPY gradle.properties /tmp/
COPY settings.gradle.kts /tmp/
COPY src/ /tmp/src

WORKDIR /tmp/

RUN gradle bootJar

FROM gradle:latest

RUN mkdir /opt/bookshelf
COPY --from=build /tmp/build/libs/*.jar /opt/bookshelf

RUN useradd -ms /bin/bash bookshelf

USER bookshelf
WORKDIR /opt/bookshelf

EXPOSE 8080
CMD java -jar *.jar
