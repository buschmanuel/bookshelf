package bookshelf

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Before
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

const val jsonType = "application/json"

@SpringBootTest
@AutoConfigureMockMvc
class TestBookController {

    @Autowired
    lateinit var objectMapper: ObjectMapper

    @Autowired
    lateinit var mockMvc: MockMvc

    //@MockBean
    @Autowired
    lateinit var bookRepository: BookRepository

    private fun createBook(id: Long?, isbn: String?, name: String?, authors: String?): BookEO {
        val bookEO = BookEO()
        id?.let { bookEO.id = id }
        bookEO.isbn = isbn
        bookEO.name = name
        bookEO.authors = authors
        return bookEO
    }

    @Before
    fun setup() {
        bookRepository.deleteAll()
        //val bookToPost = createBook(1, "978-23-3-532342-1", "Buch der Bücher", "Autor der Autoren")
        //val listAll: List<BookEO> = listOf(
        //    createBook(1, "978-23-3-532342-1", "Buch der Bücher", "Autor der Autoren"),
        //    createBook(2, "978-23-3-532342-2", "AB", "AC"),
        //    createBook(2, "978-23-3-532342-3", "BB", "BC")
        //)
        //`when`(bookRepository.findAll()).thenReturn(listAll)
        //`when`(bookRepository.save<BookEO>()).thenReturn(bookToPost)
        //`when`(bookRepository.findById()).thenReturn(bookToPost)
    }

    @Test
    fun `test get list`() {
        mockMvc.perform(
            get("/api/books")
                .contentType(jsonType)
        ).andExpect(status().isOk)
    }

    @Test
    fun `test validation with missing isbn name and authors`() {
        mockMvc.perform(
            post("/api/books")
                .contentType(jsonType)
                .content(objectMapper.writeValueAsString(createBook(null, null, null, null)))
        ).andExpect(status().is4xxClientError)
            .andExpect(jsonPath("$.isbn").value("ISBN is mandatory"))
            .andExpect(jsonPath("$.name").value("Name is mandatory"))
            .andExpect(jsonPath("$.authors").value("Authors is mandatory"))
    }

    @Test
    fun `test create success`() {
        mockMvc.perform(
            post("/api/books")
                .contentType(jsonType)
                .content(
                    objectMapper.writeValueAsString(
                        createBook(
                            null,
                            "978-3-446-22309-7",
                            "Der pragmatische Programmierer",
                            "Andrew Hunt & David Thomas, übersetzt von Andreas Braig & Steffen Gemkow"
                        )
                    )
                )
        ).andExpect(status().isCreated)
            .andExpect(redirectedUrl("1"))

        mockMvc.perform(
            get("/api/books/1")
                .contentType(jsonType)
        ).andExpect(status().isOk)
            .andExpect(jsonPath("$.isbn").value("978-3-446-22309-7"))
            .andExpect(jsonPath("$.name").value("Der pragmatische Programmierer"))
            .andExpect(jsonPath("$.authors").value("Andrew Hunt & David Thomas, übersetzt von Andreas Braig & Steffen Gemkow"))
    }

}