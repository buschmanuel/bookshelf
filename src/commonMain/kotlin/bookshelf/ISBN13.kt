package bookshelf

import kotlin.math.absoluteValue

data class ISBN13(
    private val prefix: Int = 978,
    private val group: Int,
    private val publisher: Int,
    private val publication: Int
) {
    open class InvalidISBN13Exception(msg: String) : RuntimeException(msg)
    class InvalidFormatISBN13Exception : InvalidISBN13Exception("Invalid ISBN format")
    class InvalidChecksumISBN13Exception : InvalidISBN13Exception("Invalid ISBN checksum")

    companion object {

        fun fromISBN13StringNoChecksum(value: String): ISBN13 {
            try {
                val (prefix, group, publisher, publication) = value.split(' ', '-')

                if (prefix != "978" && prefix != "979") {
                    throw InvalidFormatISBN13Exception()
                }

                return ISBN13(prefix.toInt(), group.toInt(), publisher.toInt(), publication.toInt())

            } catch (e: IndexOutOfBoundsException) {
                throw InvalidFormatISBN13Exception()
            } catch (e: NumberFormatException) {
                throw InvalidFormatISBN13Exception()
            }
        }

        fun fromISBN13String(value: String): ISBN13 {
            try {
                val (_, _, _, _, checksum) = value.split(' ', '-')
                val isbn = fromISBN13StringNoChecksum(value)

                if (isbn.isValidWithChecksum(checksum.toInt())) {
                    return isbn
                }
                throw InvalidChecksumISBN13Exception()

            } catch (e: IndexOutOfBoundsException) {
                throw InvalidFormatISBN13Exception()
            } catch (e: NumberFormatException) {
                throw InvalidFormatISBN13Exception()
            }
        }
    }

    fun checksum(): Int {
        return (10 - (("$prefix$group$publisher$publication"
            .map { char -> char.toString().toInt() }
            .withIndex().map { it.value * if (it.index % 2 == 0) 1 else 3 }.sum()) % 10)).absoluteValue
    }

    fun isValidWithChecksum(checksum: Int): Boolean {
        return checksum() == checksum
    }

    override fun toString(): String {
        val checksum = checksum()
        return "$prefix-$group-$publisher-$publication-$checksum"
    }
}