package bookshelf

interface Book {
    val id: Long?
    val isbn: String?
    val name: String?
    val authors: String?
    val shortAnnotation: String?
}