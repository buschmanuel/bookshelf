package bookshelf

import bookshelf.Book
import kotlinx.serialization.Serializable

@Serializable
data class BookDTO(
    override val id: Long? = null,
    override var isbn: String? = null,
    override var name: String? = null,
    override var authors: String? = null,
    override var shortAnnotation: String? = null,
) : Book