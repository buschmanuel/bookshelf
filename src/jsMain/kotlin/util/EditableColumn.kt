package util

import kotlinx.html.InputType
import kotlinx.html.js.onClickFunction
import kotlinx.html.js.onFocusFunction
import kotlinx.html.js.onInputFunction
import kotlinx.html.js.onSubmitFunction
import kotlinx.html.style
import org.w3c.dom.HTMLInputElement
import react.*
import react.dom.*

external interface EditableColumnsProps<T> : RProps {
    var name: String
    var getter: (obj: T) -> String
    var setter: (obj: T, value: String) -> Unit
    var submit: (obj: T) -> Unit
    var editDefault: Boolean
    var validationErrorGetter: (obj: T) -> String?
    var liveValidation: (value: String) -> Boolean
}

fun <T> createFunctionalComponent(obj: T): FunctionalComponent<EditableColumnsProps<T>> {
    return functionalComponent { props ->
        div {
            val (edit, setEdit) = useState(props.editDefault)
            val (text, setText) = useState(props.getter(obj))
            attrs.onClickFunction = {
                setEdit(true)
            }
            if (edit || text.isEmpty()) {
                form {
                    attrs.onSubmitFunction = {
                        it.preventDefault()
                        setEdit(props.editDefault)
                        props.submit(obj)
                    }
                    input {
                        attrs.onFocusFunction = {
                            setEdit(true)
                        }
                        attrs.onInputFunction = {
                            val value = (it.target as HTMLInputElement).value
                            setText(value)
                            props.setter(obj, value)
                        }
                        attrs.type = InputType.text
                        attrs.placeholder = props.name
                        attrs.value = text
                        attrs.jsStyle {
                            color = if (props.liveValidation(props.getter(obj))) "black" else "red"
                        }
                    }
                    props.validationErrorGetter(obj)?.let { error ->
                        p {
                            +error
                        }
                    }
                }
            } else {
                +text
            }
        }
    }
}

fun <T> RBuilder.editableColumn(obj: T, handler: EditableColumnsProps<T>.() -> Unit) =
    child(createFunctionalComponent(obj)) {
        attrs {
            handler()
        }
    }

fun <T> editableColumn(
    context: RBuilder,
    name: String,
    getter: (obj: T) -> String,
    setter: (obj: T, value: String) -> Unit,
    submit: (obj: T) -> Unit,
    editDefault: Boolean = false,
    validationErrorGetter: (obj: T) -> String? = { null },
    liveValidation: (value: String) -> Boolean  = { true }
): Any {
    return object {
        val name = name
        val cell = { obj: T, _: Any, _: Any, _: Any ->
            context.editableColumn(obj) {
                this.name = name
                this.getter = getter
                this.setter = setter
                this.submit = submit
                this.editDefault = editDefault
                this.validationErrorGetter = validationErrorGetter
                this.liveValidation = liveValidation
            }
        }
    }
}
