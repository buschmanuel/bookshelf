import bookshelf.BookDTO
import io.ktor.client.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.utils.io.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

class BookService(private val booksApi: String) {

    val jsonClient = HttpClient {
        install(JsonFeature) { serializer = KotlinxSerializer() }
    }

    fun toSearchString(book: BookDTO): String {
        return "search_isbn=${book.isbn ?: ""}&search_name=${book.name ?: ""}&search_authors=${book.authors ?: ""}"
    }

    suspend fun getBookListFilterdBy(bookSearch: BookDTO): List<BookDTO> {
        return jsonClient.get(booksApi + "/search?" + toSearchString(bookSearch))
    }

    suspend fun getBookList(): List<BookDTO> {
        return jsonClient.get(booksApi)
    }

    suspend fun addBook(bookDTO: BookDTO, success: () -> Unit, validationSetter: (errors: BookDTO) -> Unit) {
        var response = jsonClient.post<HttpResponse>(booksApi) {
            contentType(ContentType.Application.Json)
            body = bookDTO
        }
        when(response.status.value) {
            201 -> success()
            400 -> validationSetter(Json.decodeFromString(response.content.readUTF8Line() ?: ""))
        }
    }

    suspend fun updateBook(bookDTO: BookDTO, success: () -> Unit, validationSetter: (errors: BookDTO) -> Unit) {
        val response = jsonClient.put<HttpResponse>(booksApi + "/${bookDTO.id}") {
            contentType(ContentType.Application.Json)
            body = bookDTO
        }
        when(response.status.value) {
            200 -> success()
            400 -> validationSetter(Json.decodeFromString(response.content.readUTF8Line() ?: ""))
        }

    }

    suspend fun deleteBook(bookDTO: BookDTO) {
        jsonClient.delete<Unit>(booksApi + "/${bookDTO.id}")
    }
}