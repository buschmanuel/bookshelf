@file:JsModule("react-data-table-component")
@file:JsNonModule

package datatable

import react.RClass

@JsName("default")
external val DataTable: RClass<dynamic>
