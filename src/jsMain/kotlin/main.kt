import bookshelf.BookDTO
import bookshelf.ISBN13
import datatable.DataTable
import kotlinext.js.asJsObject
import kotlinx.browser.document
import kotlinx.browser.window
import kotlinx.coroutines.*
import kotlinx.html.js.onClickFunction
import react.*
import react.dom.render
import styled.styledButton
import util.editableColumn

private val scope = MainScope()

private val bookShelfService = BookService(window.location.origin + "/api/books")

class WaitForComplete(private val scope: CoroutineScope, private val delay: Long) {
    private var delayedCompleteJob: Job? = null
    fun <T> completedSubmit(submit: (obj: T) -> Unit, obj: T) {
        delayedCompleteJob?.let { job -> job.cancel() }
        delayedCompleteJob = scope.launch {
            delay(delay)
            submit(obj)
        }
    }
}

val App = functionalComponent<RProps> { _ ->

    val (bookList, setBookList) = useState(emptyList<BookDTO>())
    val (bookSearch, setBookSearch) = useState<BookDTO?>(null)
    val (validationBook, setValidationBook) = useState<BookDTO?>(null)
    val (bookValidationErrors: BookDTO, setBookValidationErrors) = useState(BookDTO())

    val waitForComplete = WaitForComplete(scope, 500)

    useEffect(dependencies = listOf()) {
        scope.launch {
            setBookList(bookShelfService.getBookList())
        }
    }

    fun bookListReload() {
        scope.launch {
            setBookList(
                if (bookSearch != null) {
                    bookShelfService.getBookListFilterdBy(bookSearch)
                } else {
                    bookShelfService.getBookList()
                }
            )
        }
    }

    fun bookCreateReload(book: BookDTO) {
        setBookValidationErrors(BookDTO())
        setValidationBook(book)
        scope.launch {
            bookShelfService.addBook(
                book,
                { setValidationBook(null) },
                { validationErrors -> setBookValidationErrors(validationErrors) })
            bookListReload()
        }
    }

    fun bookUpdateReload(book: BookDTO) {
        setBookValidationErrors(BookDTO())
        scope.launch {
            bookShelfService.updateBook(
                book,
                { setValidationBook(null) },
                { validationErrors -> setBookValidationErrors(validationErrors) })
            bookListReload()
        }
    }

    fun bookDeleteReload(book: BookDTO) {
        scope.launch {
            bookShelfService.deleteBook(book)
            bookListReload()
        }
    }

    fun bookSearchReload(bookSearch: BookDTO) {
        setBookSearch(bookSearch)
        bookListReload()
    }

    fun <T> updateSearchReload(
        obj: T,
        value: String,
        setter: (obj: T, value: String) -> Unit,
        submit: (obj: T) -> Unit
    ) {
        setter(obj, value)
        waitForComplete.completedSubmit(submit, obj)
    }

    fun serverValidationError(book: BookDTO, getter: (obj: BookDTO) -> String): String? {
        if (book.id != bookValidationErrors.id) {
            return null
        }
        return getter(bookValidationErrors)
    }

    fun isbn13InputIsValid(isbn: String): Boolean {
        return try {
            ISBN13.fromISBN13String(isbn).toString()
            true
        } catch (e: ISBN13.InvalidISBN13Exception) {
            false
        }
    }

    val getterISBN = { book: BookDTO -> book.isbn ?: "" }
    val setterISBN = { book: BookDTO, value: String -> book.isbn = value }

    val getterName = { book: BookDTO -> book.name ?: "" }
    val setterName = { book: BookDTO, value: String -> book.name = value }

    val getterAuthors = { book: BookDTO -> book.authors ?: "" }
    val setterAuthors = { book: BookDTO, value: String -> book.authors = value }

    val getterShortAnnotation = { book: BookDTO -> book.shortAnnotation ?: "" }
    val setterShortAnnotation = { book: BookDTO, value: String -> book.shortAnnotation = value }

    // Table of books
    DataTable {
        attrs.title = "Books"
        attrs.columns = listOf(
            object {
                val name = "ISBN"
                val selector = "isbn"
            },
            editableColumn(
                this,
                "Name",
                getterName,
                setterName,
                ::bookUpdateReload,
                false,
                { obj -> serverValidationError(obj, getterName) }
            ),
            editableColumn(
                this,
                "Authors",
                getterAuthors,
                setterAuthors,
                ::bookUpdateReload,
                false,
                { obj -> serverValidationError(obj, getterAuthors) }
            ),
            editableColumn(
                this,
                "Short annotation",
                getterShortAnnotation,
                setterShortAnnotation,
                ::bookUpdateReload
            ),
            object {
                val cell = { row: BookDTO, _: Any, _: Any, _: Any ->
                    this@functionalComponent.styledButton {
                        +"Delete"
                        attrs.onClickFunction = { bookDeleteReload(row) }
                    }
                }
            }).map { col -> col.asJsObject() }.toTypedArray()
        attrs.data = bookList.map { entry -> entry.asJsObject() }.toTypedArray()
    }

    // Table for search
    DataTable {
        attrs.noHeader = true
        attrs.noTableHead = true
        attrs.columns = listOf(
            editableColumn(
                this,
                "Search ISBN",
                getterISBN,
                { book: BookDTO, value ->
                    updateSearchReload(book, value, setterISBN, ::bookSearchReload)
                },
                { }, true
            ),
            editableColumn(
                this,
                "Search Name",
                getterName,
                { book: BookDTO, value ->
                    updateSearchReload(book, value, setterName, ::bookSearchReload)
                },
                { }, true
            ),
            editableColumn(
                this,
                "Search Authors",
                getterAuthors,
                { book: BookDTO, value ->
                    updateSearchReload(book, value, setterAuthors, ::bookSearchReload)
                },
                { }, true
            ),
            {},
            {}).map { col -> col.asJsObject() }.toTypedArray()
        attrs.data = listOf(bookSearch ?: BookDTO).map { entry -> entry.asJsObject() }.toTypedArray()
    }

    // Table for create
    DataTable {
        attrs.title = "New book"
        attrs.noTableHead = true
        attrs.columns = listOf(
            editableColumn(
                this,
                "ISBN (XXX-X*-X*-X*-X*)",
                getterISBN,
                setterISBN,
                ::bookCreateReload, true,
                { obj -> serverValidationError(obj, getterISBN) },
                { value -> isbn13InputIsValid(value) }
            ),
            editableColumn(
                this,
                "Name",
                getterName,
                setterName,
                ::bookCreateReload, true,
                { obj -> serverValidationError(obj, getterName) }
            ),
            editableColumn(
                this,
                "Authors",
                getterAuthors,
                setterAuthors,
                ::bookCreateReload, true,
                { obj -> serverValidationError(obj, getterAuthors) }
            ),
            editableColumn(
                this,
                "Short annotation",
                getterShortAnnotation,
                setterShortAnnotation,
                ::bookCreateReload, true
            ),
            object {
                val cell = { row: BookDTO, _: Any, _: Any, _: Any ->
                    this@functionalComponent.styledButton {
                        +"Create"
                        attrs.onClickFunction = { bookCreateReload(row) }
                    }
                }
            }).map { col -> col.asJsObject() }.toTypedArray()
        attrs.data = listOf<BookDTO>(validationBook ?: BookDTO()).map { entry -> entry.asJsObject() }.toTypedArray()
    }
}

fun main() {
    render(document.getElementById("root")) {
        child(App)
    }
}