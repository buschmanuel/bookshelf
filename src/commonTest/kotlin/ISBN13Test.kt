import bookshelf.ISBN13
import kotlin.js.JsName
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class ISBN13Test {

    @Test
    fun `test_generate_checksum`() {
        assertEquals(9, ISBN13(978, 3, 86680, 192).checksum())
        assertEquals(7, ISBN13(978, 3, 86682, 192).checksum())
        assertEquals(3, ISBN13(978, 1, 86682, 192).checksum())
    }

    @Test
    fun `test_fromISBN13String`() {
        assertEquals("978-3-86680-192-9", ISBN13.fromISBN13String("978-3-86680-192-9").toString())
    }

    @Test
    fun `test_invalid_ISBN13_format_with_fromISBN13String`() {
        assertFailsWith<ISBN13.InvalidFormatISBN13Exception> {
            assertEquals("978-3-86680-192-9", ISBN13.fromISBN13String("978-3-86680-192").toString())
        }
        assertFailsWith<ISBN13.InvalidFormatISBN13Exception> {
            ISBN13.fromISBN13String("978-3-86680-192-X")
        }
    }

    @Test
    fun `test_complete_ISBN13_checksum_with_fromISBN13StringNoChecksum`() {
        ISBN13.fromISBN13StringNoChecksum("978-3-86680-192-X")
        assertEquals("978-3-86680-192-9", ISBN13.fromISBN13StringNoChecksum("978-3-86680-192").toString())
    }

    @Test
    fun `test_invalid_ISBN13_format_with_fromISBN13StringNoChecksum`() {
        assertFailsWith<ISBN13.InvalidFormatISBN13Exception> {
            ISBN13.fromISBN13StringNoChecksum("978-3-86680-X-X")
        }
    }

    @Test
    fun `test_invalid_ISBN13_checksum_with_fromISBN13String`() {
        assertFailsWith<ISBN13.InvalidChecksumISBN13Exception> {
            ISBN13.fromISBN13String("978-3-86680-192-4")
        }
    }
}