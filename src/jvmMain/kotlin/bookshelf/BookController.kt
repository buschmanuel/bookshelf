package bookshelf

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.data.jpa.domain.Specification.where
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.http.ResponseEntity
import org.springframework.validation.FieldError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.*
import java.net.URI
import javax.validation.Valid

private const val basePath = "api/books"

@RestController
@RequestMapping(basePath)
open class BookController constructor(
    private val bookRepository: BookRepository,
    private val objectMapper: ObjectMapper
) {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException::class)
    fun handleValidationException(ex: MethodArgumentNotValidException): Map<String, String> {
        val errors: MutableMap<String, String> = mutableMapOf()
        if (ex.target is BookEO) {
            (ex.target as BookEO).id?.let { id -> errors.put("id", "$id") }
        }
        ex.bindingResult.allErrors.forEach { err -> errors[(err as FieldError).field] = err.defaultMessage ?: "" }
        return errors
    }

    @GetMapping
    fun getAll(): List<Book> {
        return bookRepository.findAll().toList()
    }

    @GetMapping("/search")
    fun search(
        @RequestParam(name = "search_isbn") searchIsbn: String? = "",
        @RequestParam(name = "search_name") searchName: String? = "",
        @RequestParam(name = "search_authors") searchAuthors: String? = "",
    ): List<Book> {
        return bookRepository.findAll(where(BookSpecification.search(searchIsbn, searchName, searchAuthors))).toList()
    }

    @GetMapping("/{id}")
    fun get(@PathVariable id: Long): BookEO {
        return bookRepository.findByIdOrNull(id) ?: throw RuntimeException()
    }

    @PostMapping(consumes = ["application/json"])
    @ResponseStatus(HttpStatus.OK)
    fun create(@Valid @RequestBody book: BookEO): ResponseEntity<String> {
        if (bookRepository.findAll(where(BookSpecification.`is`("isbn", book.isbn ?: ""))).isNotEmpty()) {
            var errorObj = BookDTO()
            errorObj.isbn = "ISBN exists already"
            return ResponseEntity.status(BAD_REQUEST).body(objectMapper.writeValueAsString(errorObj))
        }

        //Normalize the ISBN13 and check checksum
        try {
            book.isbn = book.isbn?.let { isbn -> ISBN13.fromISBN13String(isbn).toString() }
        } catch(e: ISBN13.InvalidChecksumISBN13Exception) {
            var errorObj = BookDTO()
            errorObj.isbn = "Invalid ISBN13 checksum"
            return ResponseEntity.status(BAD_REQUEST).body(objectMapper.writeValueAsString(errorObj))
        }

        val savedBook = bookRepository.save(book)
        return ResponseEntity.created(URI("${savedBook.id}")).build()
    }

    @PutMapping("/{id}", consumes = ["application/json"])
    @ResponseStatus(HttpStatus.OK)
    fun update(@PathVariable id: Long, @Valid @RequestBody book: BookEO): ResponseEntity<String> {
        var bookEO = bookRepository.findByIdOrNull(id) ?: throw RuntimeException()
        bookEO.isbn = book.isbn
        bookEO.name = book.name
        bookEO.authors = book.authors
        bookEO.shortAnnotation = book.shortAnnotation
        bookRepository.save(bookEO)
        return ResponseEntity.ok().build()
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun delete(@PathVariable id: Long) {
        bookRepository.deleteById(id)
    }
}