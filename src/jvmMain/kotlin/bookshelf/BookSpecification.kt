package bookshelf

import org.springframework.data.jpa.domain.Specification
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Root

class BookSpecification {
    companion object {
        fun containing(name: String, value: String): Specification<BookEO> {
            return Specification<BookEO> { root: Root<BookEO>, _: CriteriaQuery<*>, cb: CriteriaBuilder ->
                cb.like(root.get<String>(name), "%$value%")
            }
        }

        fun `is`(name: String, value: String): Specification<BookEO> {
            return Specification<BookEO> { root: Root<BookEO>, _: CriteriaQuery<*>, cb: CriteriaBuilder ->
                cb.like(root.get<String>(name), "$value")
            }
        }

        fun search(searchIsbn: String?, searchName: String?, searchAuthors: String?): Specification<BookEO>? {
            var search: Specification<BookEO>? = Specification.where(null)
            search = search?.and(searchIsbn?.ifBlank { null }?.let { value -> containing("isbn", value) })
            search = search?.and(searchName?.ifBlank { null }?.let { value -> containing("name", value) })
            search =
                search?.and(searchAuthors?.ifBlank { null }?.let { value -> containing("authors", value) })
            return search
        }
    }
}