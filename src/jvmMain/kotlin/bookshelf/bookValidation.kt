package bookshelf

import javax.validation.Constraint
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import javax.validation.Payload
import kotlin.reflect.KClass

@Constraint(validatedBy = [ISBNFormatValidator::class])
@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
annotation class ISBN13Format(
    val message: String = "Invalid ISBN13 format",
    val groups: Array<KClass<Any>> = [],
    val payload: Array<KClass<out Payload>> = []
)

class ISBNFormatValidator : ConstraintValidator<ISBN13Format, String> {
    override fun isValid(value: String?, context: ConstraintValidatorContext?): Boolean {
        return try {
            value?.let { ISBN13.fromISBN13String(value) }.let { true }
        } catch (e: ISBN13.InvalidFormatISBN13Exception) {
            false
        } catch (e: ISBN13.InvalidISBN13Exception) {
            // We only check for format
            return true
        }
    }
}
