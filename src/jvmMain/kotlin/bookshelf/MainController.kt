package bookshelf

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping
open class MainController {

    @RequestMapping
    fun main(): String {
        return "index.html"
    }
}