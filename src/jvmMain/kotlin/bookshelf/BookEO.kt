package bookshelf

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Pattern

@Entity
class BookEO : Book {
    @Id
    @GeneratedValue
    override var id: Long? = null

    @Column(unique = true)
    @NotBlank(message = "ISBN is mandatory")
    @ISBN13Format
    override var isbn: String? = null

    @NotBlank(message = "Name is mandatory")
    override var name: String? = null

    @NotBlank(message = "Authors is mandatory")
    override var authors: String? = null
    override var shortAnnotation: String? = null
}