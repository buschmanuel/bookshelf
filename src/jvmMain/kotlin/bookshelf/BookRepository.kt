package bookshelf

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface BookRepository : CrudRepository<BookEO, Long>, JpaRepository<BookEO, Long>, JpaSpecificationExecutor<BookEO>
