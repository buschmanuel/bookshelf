package bookshelf

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@EnableAutoConfiguration
@SpringBootApplication
@EnableJpaRepositories
open class BookShelfApplication

fun main(args: Array<String>) {
    runApplication<BookShelfApplication>(*args)
}