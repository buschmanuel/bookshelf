BookShelf
=========

Simple BookShelf CRUD web application.

Run the app with gradle (http://localhost:8080):

```
./gradlew bootRun
```

Run the app with java (http://localhost:8080):

```
./gradlew bootJar
java -jar build/libs/BookShelf-1.0-SNAPSHOT.jar.jar
```

Run the app with docker (http://localhost:8080):

```
docker build . -t bookshelf
docker run -it --rm -P bookshelf:latest
```
