val kotlinVersion = "1.4.10"
val ktorVersion = "1.4.1"
val serializationVersion = "1.0.1"
val springStarterVersion = "2.4.0"
val jupiterVersion = "5.6.0"

plugins {
    val kotlinVersion = "1.4.10"
    val springStarterVersion = "2.4.0"
    kotlin("multiplatform") version "$kotlinVersion"
    kotlin("plugin.serialization") version "$kotlinVersion"
    id("org.springframework.boot").version("$springStarterVersion")
    application
}

buildscript {
    dependencies {
        classpath(fileTree("libs/gradle-plugins/kotlin"))
    }
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    maven("https://kotlin.bintray.com/ktor")
    maven("https://kotlin.bintray.com/kotlin-js-wrappers/")
    jcenter()
    mavenCentral()
}

kotlin {
    /* Targets configuration omitted. 
    *  To find out how to configure the targets, please follow the link:
    *  https://kotlinlang.org/docs/reference/building-mpp-with-gradle.html#setting-up-targets */

    jvm {
        withJava()

        tasks.test {
            useJUnitPlatform()
            jvmArgs("--enable-preview")
        }
    }

    js {
        browser {
            binaries.executable()
        }
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(kotlin("stdlib-common"))
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$serializationVersion")
                implementation("io.ktor:ktor-client-core:$ktorVersion")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
                implementation(kotlin("test-junit5"))
            }
        }

        val jvmMain by getting {
            dependencies {
                api("jakarta.xml.bind:jakarta.xml.bind-api:2.3.2")

                implementation("org.springframework.boot:spring-boot-loader:$springStarterVersion")
                implementation("org.springframework.boot:spring-boot-starter:$springStarterVersion")
                implementation("org.springframework.boot:spring-boot-starter-web:$springStarterVersion")
                implementation("org.springframework.boot:spring-boot-starter-tomcat:$springStarterVersion")
                implementation("org.springframework.boot:spring-boot-starter-data-jpa:$springStarterVersion")
                implementation("org.springframework.boot:spring-boot-starter-validation:$springStarterVersion")
                implementation("org.jetbrains.kotlin:kotlin-reflect:$kotlinVersion") // required by data-jpa
                implementation("com.h2database:h2:1.4.200") // for memory storage
                implementation("mysql:mysql-connector-java:8.0.22") // for docker-compose persistence

                implementation("org.springdoc:springdoc-openapi-ui:1.5.0")
            }
        }

        val jvmTest by getting {
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-junit"))
                implementation("io.kotlintest:kotlintest-runner-junit5:3.3.2")
                implementation("org.springframework.boot:spring-boot-starter-test:$springStarterVersion")
                implementation("org.mockito:mockito-junit-jupiter:2.23.0")
            }
        }

        val jsMain by getting {
            dependencies {
                implementation(kotlin("stdlib-js"))

                implementation("org.jetbrains:kotlin-react:17.0.0-pre.126-kotlin-1.4.10")
                implementation("org.jetbrains:kotlin-react-dom:17.0.0-pre.126-kotlin-1.4.10")
                implementation("org.jetbrains:kotlin-styled:5.2.0-pre.126-kotlin-1.4.10")

                implementation("io.ktor:ktor-client-js:$ktorVersion") //include http&websockets

                //ktor client js json
                implementation("io.ktor:ktor-client-json-js:$ktorVersion")
                implementation("io.ktor:ktor-client-serialization-js:$ktorVersion")

                implementation(npm("react", "17.0.0"))
                implementation(npm("react-dom", "17.0.0"))

                implementation(npm("style-loader", "2.0.0"))
                implementation(npm("css-loader", "2.0.0"))
                implementation(npm("webpack", "4.42.1"))
                implementation(npm("process", "0.11.10"))

                implementation(npm("react-data-table-component", "6.11"))

                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.0")
            }
        }

        val jsTest by getting {
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-js"))
            }
        }
    }
}

application {
    mainClassName = "bookshelf.BookShelfApplicationKt"
}

tasks.named<Test>("jvmTest") {
    useJUnitPlatform()
}

// Disable javascript tests as they cause problems on build
tasks.named<Task>("jsBrowserTest") {
    enabled = false
}

// include JS artifacts in any JAR we generate
tasks.withType<Jar> {
    val taskName = if (project.hasProperty("isProduction")) {
        "jsBrowserProductionWebpack"
    } else {
        "jsBrowserDevelopmentWebpack"
    }
    val webpackTask = tasks.getByName<org.jetbrains.kotlin.gradle.targets.js.webpack.KotlinWebpack>(taskName)
    dependsOn(webpackTask) // make sure JS gets compiled first
    from(File(webpackTask.destinationDirectory, webpackTask.outputFileName)) {
        into("BOOT-INF/classes/static")
    } // bring output file along into the JAR
}

tasks {
    withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }
}

distributions {
    main {
        contents {
            from("$buildDir/libs") {
                rename("${rootProject.name}-jvm", rootProject.name)
                into("lib")
            }
        }
    }
}

tasks.withType<JavaExec> {
    classpath(tasks.getByName<Jar>("jvmJar")) // so that the JS artifacts generated by `jvmJar` can be found and served
}

